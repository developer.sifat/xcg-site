from django.contrib import admin
from django.contrib.sites.models import Site
from django.contrib import admin
# Register your models here.
from src.models import ServiceType, ServiceOrder

admin.site.site_header="Webteaker"
admin.site.site_title="Mazegeek Incorporation"

class ServiceTypeAdmin(admin.ModelAdmin):
    list_display = ['name','description']
    list_per_page = 25

admin.site.register(ServiceType,ServiceTypeAdmin)

class ServiceOrderAdmin(admin.ModelAdmin):
    list_per_page = 25

admin.site.register(ServiceOrder,ServiceOrderAdmin)