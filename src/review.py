from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import ModelForm
from django.template.loader import render_to_string

from src.forms import ServiceOrderForm, AppointmentForm, ReviewForm
from src.models import ServiceOrder


def save_review_form(request,form,template_name):
    data=dict()
    if request.method=='POST':
        if form.is_valid():
            form.save()
            data['form_is_valid']=True
        else:
            data['form_is_valid']=False
    context={'form':form}
    data['html_form']=render_to_string(template_name,context,request=request)
    return JsonResponse(data)

def review_create(request):
    if request.method=='POST':
        form=ReviewForm(request.POST)
    else:
        form=ReviewForm()
    return save_review_form(request,form,"src/partials/review.html")

